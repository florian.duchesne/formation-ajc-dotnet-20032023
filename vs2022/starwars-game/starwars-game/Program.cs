﻿using starwars_game;
using System.Reflection.Metadata.Ecma335;

Console.ForegroundColor = ConsoleColor.DarkGreen;

// string monTitre = "a starwars game".ToUpper();
var monTitre = "a starwars game".ToUpper();

// monTitre = monTitre + " (" + DateTime.Now.ToString() + ")";
//string format = "{0} ({1})";
//monTitre = string.Format(format, monTitre, DateTime.Now);
//Console.WriteLine(monTitre);

// Code identique
// Console.WriteLine(format, monTitre, DateTime.Now);

Console.WriteLine($"{monTitre.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
Console.ForegroundColor = ConsoleColor.DarkCyan;
var sousTitre = "A zelda copy cat";
Console.WriteLine(sousTitre);
Console.ForegroundColor = ConsoleColor.White;

AfficherMenu();


void AfficherMenu()
{
    //string[] menu = { // new string[] 
    //    "1. Nouvelle partie",
    //    "2. Charger partie",
    //    "0. Quitter"
    //};

    //foreach (var item in menu)
    //{
    //    Console.WriteLine(item);
    //}

    var listMenu = Enum.GetValues(typeof(MenuItemType));
    foreach (var item in listMenu)
    {
        Console.WriteLine(item);
        string itemTransforme = item.ToString().Replace("_", " ");
        Console.WriteLine("{0}: {1}", (int)item, itemTransforme);
    }

    //Type type = typeof(string);
    //foreach(var item in type.GetMethods())
    //{
    //    Console.WriteLine(item);
    //}
}

int ChoisirMenu()
{
    int menuItem = 0;

    do
    {
        AfficherMenu();

        Console.WriteLine("Ton choix ?");
        menuItem = int.Parse(Console.ReadLine());

        //const int DEMARRER_PARTIE_INDEX = 1;

        MenuItemType monChoix = MenuItemType.Quitter;

        switch ((MenuItemType) menuItem)
        {
            case MenuItemType.DemarrerPartie:
                {
                    DemarrerPartie();
                }
                break;

            case MenuItemType.ChargerPartie:
                {
                    ChargerPartie();
                }
                break;
        }
    } while (menuItem != 0);

    return menuItem;
}

void DemarrerPartie()
{
    Console.WriteLine("Ton prénom stp ?");
    var prenom = Console.ReadLine();

    // DateTime maVraiDate;
    bool dateValide = false;
    do
    {
        Console.WriteLine("Ta date de naissance stp ?");
        var dateDeNaissance = Console.ReadLine();

        if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
        {
            var comparaisonDates = DateTime.Now - maVraiDate;
            int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
            Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

            dateValide = true;

            int ageSaisi = DemanderAge();
        }
        else
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
    while (!dateValide);
}

int DemanderAge()
{
    bool ageValide = false;
    int vraiAge = 0;

    while (!ageValide)
    {
        Console.WriteLine("Ton age stp ?");
        var age = Console.ReadLine();

        ageValide = int.TryParse(age, out vraiAge);

        if (ageValide)
        {
            ageValide = vraiAge > 13;
        }
    }

    return vraiAge;
}

void ChargerPartie()
{

}

//ChoisirMenu();
PreparerEnnemis();

void PreparerEnnemis()
{
    //List<string> noms = new List<string>();
    //var stop = false;
    //do
    //{

    //    Console.WriteLine("Ajoute un ennemi (rentre STOP pour arrêter)");
    //    string ajout = Console.ReadLine();
    //    noms.Add(ajout);
    //    foreach (var ennemi in noms)
    //    {
    //        if (ajout == ennemi)
    //        {
    //            noms.Remove(ajout);
    //        }
    //        Console.WriteLine(ennemi);
    //    }
    //    if (ajout == "STOP")
    //    {
    //        stop = true;
    //    }
    //}

    //while (!stop);

    //foreach (var ennemi in noms)
    //{
  
    //    Console.WriteLine(ennemi);
    //}


    //foreach noms{ }
    //Console.WriteLine(noms.);


    //string[] StrNoms =
    //{
    //    "Dark Vador",
    //        "Boba fet",
    //        "Empereur Palpatine"
    //};

    //string[][] tabTab;
    //var item = tabTab[0][0];

    string[,] multiDim = new string[2, 3];
    var item = multiDim[0, 0];

    //StrNoms[0] = "Darth Vader";

    List<string> noms = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine",
    };
    //noms.Add("Droide");
    //noms.Remove("Droide");

    //var ennemi = noms[0];
    //int index = noms.IndexOf("Boba fet");
    //ennemi = noms[index];

    //List<int> powers = new List<int>()
    //{
    //    1, 2, 5, 10, 20
    //};

    //var sum = powers.Sum(); // Linq
    //var first = powers.Min();


    //var min = StrNoms.Min();

    DemandeAjoutEnnemisParUser(noms);

    var query = from enem in noms
                let eneMaj = enem[0].ToString().ToUpper() + enem.Substring(1).ToLower()
                let premierCar = enem[0]
                let monObjet = new {Maj = eneMaj, Initial = enem }
                where enem.ToUpper().StartsWith("D") || enem.ToUpper().EndsWith("D")
                orderby enem descending
                select monObjet;

    foreach (var enemy in query)
    {
        Console.WriteLine(enemy.Maj);
        Console.WriteLine(enemy.Initial);
    }

    //// on peut créer un objet sans type, à la volée (il a un type anonyme)
    //var monObjeALaVolee = new
    //{
    //    Prenom = "Anakin",
    //};
    //Console.WriteLine(monObjeALaVolee.Prenom);

    //var query2 = noms.Where(item => item.StartsWith("D") && item.EndsWith("D"))
    //.OrderByDescending(item => item)
    //.Select(item => item);

    //query = query.Where(item => item.EndsWith("E"));

}

// Version Evan :

void DemandeAjoutEnnemisParUser(List<string> lesEnnemis)
{
    const string ARRET_SAISIE = "STOP";
    string valeurSaisie = "";
    bool demandeArret = false;
    do
    {
        Console.WriteLine("Nouvel ennemi ? (STOP pour arrêter)");
        valeurSaisie = Console.ReadLine();
        demandeArret = valeurSaisie == ARRET_SAISIE;
        if ( !demandeArret && !lesEnnemis.Contains(valeurSaisie))
        {
            lesEnnemis.Add(valeurSaisie);
        }
    } while(! demandeArret);
};