﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game
{
    public enum MenuItemType
    {
        DemarrerPartie = 1,
        ChargerPartie = 2,
        Options = 3,
        Quitter = 0
    }
}
