﻿using System.ComponentModel.Design;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using exo6.api;    

namespace ConsoleAppExo6
{
    internal class Program
    {
        static void Main(string[] args)
        {

            BasePersonnage persoPrincipal = new Personnage(Console.WriteLine, Console.ReadLine);

            while (true)
            {
                persoPrincipal.SeDeplacer();
            }

            persoPrincipal.SeDeplacer();

            //position2D position = new() { X = 2, Y = 2 };
            //position2D position2 = new() { Y = 2, X = 2 };

            Position2D position = new(2, 2);
            Position2D position2 = new(2, 2);


            Console.WriteLine(position == position2);

            var game = new Partie();

            game.Demarrer();

            try
            {
                game.Sauvegarder();
            }
            catch (NotImplementedException ex)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Oops erreur !", ex.Message);
                Console.ForegroundColor = ConsoleColor.White;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Console.WriteLine("Execute obligatoire");
            }
            //catch (Exception ex)
            //{
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine("Oops erreur", ex.Message);
            //    Console.ForegroundColor = ConsoleColor.White;
            //}

            //List<string> elements = new List<string>()
            //{
            //"1. Nouvelle partie",
            //"2. Charger partie",
            //"3. quitter",
            //};

            //Menu menu = new Menu("A star wars game", "copycat of zelda !", elements);

            //menu.afficherMenu();

            //menu.choixJoueur();

            //menu.PreparerEnnemis();

        }
    }
}