﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace menu.api
{
    public class MenuItem
    {
        public int OrdreAffichage { get; set; }
        public int Id { get; set; }
        public string Libelle { get; set; }

        public override string ToString()
        {
            return $"{this.Id} : {this.Libelle}";
        }

    }

}
