﻿using menu.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api
{

    public class Menu
    {

        private readonly Action<string> afficherInfo;
        private readonly Func<string> recupererInfo;
        const int DEMARRER_PARTIE_INDEX = 1;
        const int CHARGER_PARTIE_INDEX = 2;
        const int QUITTER_PARTIE_INDEX = 3;
        const int VALIDER_PARTIE = 4;

        private string nomJeu;
        private string sousTitre;
        public List<MenuItem> Items { get; private set; } = new();
        //private List<Partie>? partiesMenu;

        public Menu(Action<string> afficher, Func<string> recupererInfo)
        {
            this.afficherInfo = afficher;
            this.recupererInfo = recupererInfo;
        }

        public void Afficher()
        {
            //this.Items.ForEach(item =>
            //{
            //    this.afficherInfo(item.ToString());
            //});
            foreach (var item in this.Items.OrderBy(item => item.OrdreAffichage))
            {
                this.afficherInfo(item.ToString());
            }
        }
        public void Ajouter(MenuItem item)
        {
            this.Items.Add(item);
        }

        public string NomJeu { get => nomJeu; set => nomJeu = value; }
        public string SousTitre { get => sousTitre; set => sousTitre = value; }
        //public List<Partie> PartiesMenu { get => partiesMenu; set => partiesMenu = value; }

        //public void afficherMenu()
        //{
        //    Console.ForegroundColor = ConsoleColor.DarkGreen;
        //    Console.WriteLine($"{this.nomJeu.ToUpper()} ({DateTime.Now.ToString("yyyy.MM.dd dddd")}) !");
        //    Console.ForegroundColor = ConsoleColor.DarkCyan;
        //    Console.WriteLine(this.sousTitre);
        //    Console.ForegroundColor = ConsoleColor.Gray;
        //    foreach (var item in elementsMenu)
        //    {
        //        Console.WriteLine(item);
        //    }
        //}

        public void choixJoueur()
        {

            var choix = int.Parse(Console.ReadLine());

            switch (choix)
            {
                case DEMARRER_PARTIE_INDEX:
                    {
                        nouvellePartie();
                    }
                    break;

                case CHARGER_PARTIE_INDEX:
                    {
                        afficherParties();
                    }
                    break;
                case QUITTER_PARTIE_INDEX:
                    {
                        QuitterJeu();
                    }
                    break;
                case VALIDER_PARTIE:
                    {
                        ChargerPartie();
                    }
                    break;

            }


        }

        public void nouvellePartie()
        {
            Console.WriteLine("Ton prénom stp ?");
            var prenom = Console.ReadLine();

            bool dateValide = false;
            do
            {
                Console.WriteLine("Ta date de naissance stp ?");
                var dateDeNaissance = Console.ReadLine();

                if (DateTime.TryParse(dateDeNaissance, out var maVraiDate))
                {
                    var comparaisonDates = DateTime.Now - maVraiDate;
                    int nbAnnees = (int)(comparaisonDates.TotalDays / 365);
                    Console.WriteLine($"Ton prénom est bien ? {prenom}, et tu es âgé de {nbAnnees} années ?");

                    dateValide = true;

                    int ageSaisi = DemanderAge();
                    //Joueur joueur = new Joueur(prenom, maVraiDate, ageSaisi);

                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("Erreur de saisie de date, ré-essaie stp !");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            while (!dateValide);

            int DemanderAge()
            {
                bool ageValide = false;
                int vraiAge = 0;

                while (!ageValide)
                {
                    Console.WriteLine("Ton age stp ?");
                    var age = Console.ReadLine();

                    ageValide = int.TryParse(age, out vraiAge);

                    if (ageValide)
                    {
                        ageValide = vraiAge > 13;
                    }
                }

                return vraiAge;
            }
        }

        public void afficherParties()
        {
        }
        public void ChargerPartie()
        {

        }
        public void QuitterJeu()
        {

        }

        //public void PreparerEnnemis()
        //{

        //    List<Ennemi> ennemis = new List<Ennemi>()
        //    {
        //    };

        //    DemandeAjoutEnnemisParUser(ennemis);

        //    var query = from enem in ennemis
        //                    //where enem.Name.ToUpper().StartsWith("D") || enem.ToUpper().EndsWith("D")
        //                orderby enem descending
        //                select enem;

        //    foreach (var enemy in query)
        //    {
        //        Console.WriteLine(enemy);
        //    }
        //}

        //void DemandeAjoutEnnemisParUser(List<Ennemi> ennemis)
        //{
        //    const string ARRET_SAISIE = "STOP";
        //    string valeurSaisie = "";
        //    bool demandeArret = false;
        //    do
        //    {
        //        Console.WriteLine("Nouvel ennemi ? (STOP pour arrêter)");
        //        valeurSaisie = Console.ReadLine();
        //        demandeArret = valeurSaisie == ARRET_SAISIE;
        //        //if (!demandeArret)
        //        //{
        //        //    Ennemi ennemi = new Ennemi(valeurSaisie);
        //        //}
        //    } while (!demandeArret);
        //}

    }
}
