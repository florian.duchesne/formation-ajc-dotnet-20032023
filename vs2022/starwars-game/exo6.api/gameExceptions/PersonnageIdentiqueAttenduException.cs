﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api.Exceptions
{
    /// <summary>
    /// A déclencher quand deux persos de jeux sont identiques
    /// </summary>
    public class PersonnageIdentiqueAttenduException : Exception
    {
    }
}
