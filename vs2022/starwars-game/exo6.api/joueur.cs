﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api
{
    public class Joueur
    {
        //private string prenom;
        //private DateTime dateNaissance;
        //private int age;


        /// <summary>
        /// Classe représentant le joueur dans le jeu
        /// </summary>
            private List<Partie> gameList = new();

            /// <summary>
            /// Liste en lecture seule des parties du joueur
            /// </summary>
            public  List<Partie> GameList { get => gameList; private set => gameList = value; }
            //this.Prenom = prenom;
            //this.DateNaissance = dateNaissance;
            //this.Age = age;
        }

        //public string Prenom { get => prenom; set => prenom = value; }
        //public DateTime DateNaissance { get => dateNaissance; set => dateNaissance = value; }
        //public int Age { get => age; set => age = value; }
    }


