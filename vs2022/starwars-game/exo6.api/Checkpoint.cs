﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api
{
    /// <summary>
    /// Sauvegarde de la partie à un instant donné
    /// </summary>
    public class Checkpoint
    {
        #region Constructors
        public Checkpoint( int gameId, int persoPointsDeVie)
        {
            this.dateCP = DateTime.Now;
        }

        #endregion

        #region Properties

        public int PersoPointsDeVie { get; set; }
        public int Id { get; set; }
        #endregion


        public DateTime dateCP;

        //public Checkpoint(DateTime dateCP, string name, int pv = 0, int coordonneesX = 0, int coordonneesY = 0) : base(name, pv, coordonneesX, coordonneesY)
        //{
        //    this.dateCP = DateTime.Now;
        //}

    }
}
