﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api
{
    /// <summary>
    /// Personnage principal, avec des spécificités sur l'attaque
    /// </summary>
    public class Personnage : BasePersonnage
    {
        private readonly Action<string> AfficherInformation;
        private readonly Func<string> recupererSaisie;
        private static Dictionary<string, Position2D> deplacements = new()
        {
            { "Q", new (-1, 0) },
            { "D", new (1, 0) },
            { "Z", new (0, -1) },
            { "S", new (0, 1) },
        };

        public Personnage(Action<string> afficherInformation, Func<string> recupererSaisie) 
        {
            this.AfficherInformation = afficherInformation;
            this.recupererSaisie = recupererSaisie;
        }
        //public override void Attaquer(BasePersonnage ennemi)
        //{
        //    base.Attaquer(ennemi);
        //}

        public override void SeDeplacer()
        {
            this.AfficherInformation("Direction ? (QZSD)");
            string saisie = this.recupererSaisie();
            if (saisie != null)
            {
                var vecteur = this.ConvertirPosition(saisie);
                this.Position = new(this.Position.X + vecteur.X, this.Position.Y);
            }
        }

        private Position2D ConvertirPosition(string direction)
        {
            return Personnage.deplacements[direction];

        }

        protected override int DefinirPointsAttaque()
        {
            var pointsAttaque = base.DefinirPointsAttaque();
            pointsAttaque += this.Bouclier;

            return pointsAttaque;
        }

        //private string name;
        //private int pv;
        //private int coordonneesX;
        //private int coordonneesY;
        //private Checkpoint[]? checkpoints;

        //public Personnage(string name, int pv = 0, int coordonneesX = 0, int coordonneesY = 0)
        //{
        //    this.name = name;
        //}

        //public string Name { get => name; set => name = value; }
        //public int Pv { get => pv; set => pv = value; }
        //public int CoordonneesX { get => coordonneesX; set => coordonneesX = value; }
        //public int CoordonneesY { get => coordonneesY; set => coordonneesY = value; }
    }
}
