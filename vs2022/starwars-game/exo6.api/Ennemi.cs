﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace exo6.api
{
    /// <summary>
    /// L'ennemi que le joueur va rencontrer
    /// </summary>
    public class Ennemi : BasePersonnage
    {
        //public override void Attaquer(BasePersonnage ennemi)
        //{
        //    base.Attaquer(ennemi);
        //}

        //public Ennemi(string name, int pv = 0, int coordonneesX = 0, int coordonneesY = 0) : base(name, pv, coordonneesX, coordonneesY) { }
                

        //public void Attaquer()
        //{
        //    Console.WriteLine("J'attaque !");
        //}

        public void SeDefendre()
        {
            Console.WriteLine("Je me défends !");
        }

        private static Random _Random = new Random();

        public override void SeDeplacer()
        {
            int nouveauX = this.Position.X + Ennemi._Random.Next(-1, 1);
            int nouveauY = this.Position.Y + Ennemi._Random.Next(-1, 1);

            this.Position = new Position2D(nouveauX, nouveauY);
        }

    }
}
