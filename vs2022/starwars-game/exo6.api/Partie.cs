﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace exo6.api
{
    /// <summary>
    /// Représente une partie de jeu, avec perso et checkpoints
    /// </summary>
    public class Partie
    {
        #region Properties
        public int Id { get; set; } = 0;
        public DateTime DateCreation { get; private set; } = DateTime.Now;
        public DateTime DateDebut { get; set; } = DateTime.Now;
        public List<Checkpoint> CheckPointList { get; set; } = new();
        public BasePersonnage? PersoPrincipal { get; set; }
        #endregion

        #region Public methods
        
        /// <summary>
        /// Démarrer une nouvelle partie
        /// Crée un nouveau personnage, ...
        /// </summary>
        public void Demarrer()
        {
            this.DateDebut = DateTime.Now;
        }

        /// <summary>
        /// Créer un point de sauvegarde
        /// <exception></exception>
        /// </summary>
        public void Sauvegarder()
        {
            if (this.PersoPrincipal != null) 
            {
                Checkpoint checkPoint = new(this.Id, this.PersoPrincipal.PointsDeVie);
                this.CheckPointList.Add(checkPoint);
            }
        }
        #endregion
        //public Partie(int id, DateTime dateDebut, Checkpoint checkpoint, Personnage personnage)
        //{
        //    this.Id = id;
        //    this.DateDebut = DateTime.Now;
        //    this.checkpoint = checkpoint;
        //    this.personnage = personnage;
        //}

        //public DateTime DateDebut { get; private set; } = DateTime.Now;

     }
}
